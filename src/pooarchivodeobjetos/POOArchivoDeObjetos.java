package pooarchivodeobjetos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class POOArchivoDeObjetos {

    public static void main(String[] args){
       //ObjectInputStream entradaDeObjetos;
       try {
         // crear un archivo con Object Stream para guardar Objetos
         File archivoDeObjetos              = new File("Personas.Obj");   
         FileOutputStream salidaConObjetos  = new FileOutputStream(archivoDeObjetos);
         ObjectOutputStream salidaDeObjetos = new ObjectOutputStream(salidaConObjetos);

         // instanciar un par de objetos
         Persona p1 = new Persona(1,"PEDRO PEREZ");
         p1.setDireccion("Circunvalacion 234");
         Persona p2 = new Persona(2,"LUIS LOPEZ","Arista 34");
         Persona p3 = new Persona(3,"MARIA RUIZ","Independencia 9");

         // grabar objetos en el archivo
         salidaDeObjetos.writeObject(p1);
         salidaDeObjetos.writeObject(p2);
         salidaDeObjetos.writeObject(p3);
         
         // cerrar el flujo al archivo de salida
         salidaDeObjetos.close();
         
         // crear un flujo de entrada para leer el contenido del archivo de objetos
         File archivoDeObjetosALeer         = new File("Personas.Obj");
         FileInputStream entradaConObjetos  = new FileInputStream(archivoDeObjetosALeer);
         ObjectInputStream entradaDeObjetos = new ObjectInputStream(entradaConObjetos);

         // leer e imprimir lo que grabamos en el archivo y ahora estamos recuperando
         while((p1 = (Persona) entradaDeObjetos.readObject())!=null){
            if(p1.getId()>0)
                System.out.println(p1.getId()+" "+p1.getNombre()+" "+p1.getDireccion());
         }  

         entradaDeObjetos.close();
      }catch (IOException ex) {
           System.out.println("Final del Archivo");
      }catch (ClassNotFoundException nf){
           System.out.println("Clase no encontrada");
      }
    }
    
}
