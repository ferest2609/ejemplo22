package pooarchivodeobjetos;

import java.io.Serializable;
// La serialización es la transformación de un objeto en una secuencia de bytes
// que pueden ser posteriormente leídos para reconstruir el objeto original.
// Para poder transformar el objeto en una secuencia de bytes, el objeto debe ser serializable.
// Un objeto es serializable si su clase implementa la interface Serializable.

public class Persona implements Serializable{
    private int id;
    private String nombre;
    private String direccion;

    public Persona() {
    }

    public Persona(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Persona(int id, String nombre, String direccion) {
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return id + "," + nombre + "," + direccion;
    }
    
    public void imprimir(){
        System.out.println(toString());
    }
}
